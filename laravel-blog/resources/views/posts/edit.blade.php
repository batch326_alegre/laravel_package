@extends('layouts.app')

@section('tabName')
Edit - {{$post->title}}
@endsection
@section('content')
    <form method="POST" action="{{url('/post/'.$post->id)}}">

        @csrf
        @method('PUT')
        <div class="col-10 mx-auto">
          <label for="title" class="form-label">Title:
          </label>
          <input type="text" id="title" name="title" value="{{ $post->title }}" class="form-control" required autofocus>
        <div class="my-5">
          <label for="body" class="form-label">Content:</label>
          <textarea name="body" id="body" class="form-control" required autofocus>{{ $post->body}}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
@endsection

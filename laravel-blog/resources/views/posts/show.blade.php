@extends('layouts.app')

@section('tabName')
    {{$post->title}}
@endsection
@section('content')
	<div class = "card col-10 mx-auto">
		<div class = 'card-body'>
			<h2 class = 'card-title'>{{$post->title}}</h2>
			<p class = "card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class = 'card-subtitle text-muted mb-3'>Created at: {{$post->created_at}}</p>
			<h4>Content:</h4>
			<p class = "card-text">{{$post->body}}</p>
             <!-- Likes and Comments count-->
            <p>
                <span style="border-right: 1px #909090 solid; padding-right: 6px">Likes: {{$post->likes->count()}}</span>
                <span>Comments: {{$post->comments->count()}}</span>
            </p>
            @if(Auth::user())
                @if(Auth::id() != $post->user_id)
                    <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                        @csrf
                        @method('PUT')
                        @if($post->likes->contains('user_id', Auth::id()))
                            <button class="btn btn-danger">Unlike</button>
                        @else
                            <button class="btn btn-success">Like</button>

                        @endif
                    </form>
                @endif
                <br/>
                    <!-- Modal trigger -->
                    <button type="button" class="btn btn-primary mt-2" data-bs-toggle="modal" data-bs-target="#commentModal">
                        Comment
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" tabindex="-1" id="commentModal">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">Leave a comment</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form method="POST" action="/posts/{{$post->id}}/comment">
                                @csrf
                                <div class="modal-body">
                                    <label for="comment">Comment:</label>
                                    <textarea class="form-control" rows="4" placeholder="Add your comment here..."id="comment" name="comment"></textarea>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Comment</button>
                                  </div>
                            </form>
                          </div>
                        </div>
                      </div>
            @endif
            <br/>
			<a href="/posts" class = "btn btn-info mt-2">View all posts</a>
		</div>
	</div>
@endsection
@section('comments')
    @if(count($post->comments)>0)
        <div class="col-10 mx-auto mt-5">
            <h5>Comments:</h5>
        </div>
        @foreach ($post->comments as $comment)
            <div class = "card col-10 mx-auto mb-4">
                <div class = 'card-body'>
                    <p class="card-text text-center" style="font-size: 150%">{{$comment->body}}</p>
                    <p class = "card-subtitle text-muted text-end">Posted by: {{$comment->user->name}}</p>
                    <p class = 'card-subtitle text-muted text-end' style="font-size: 80%">posted on: {{$comment->created_at}}</p>
                </div>
            </div>
        @endforeach
    @endif
@endsection

@extends('layouts.app')

@section('tabName')
    Welcome
@endsection
@section('content')
    <div class="d-flex justify-content-center">
        <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" alt="laravel logo" width="400">
    </div>

    <div class="d-flex justify-content-center mt-3">
        <h1>Featured Posts:</h1>
    </div>
    @if(count($randomPosts)>0)
        @foreach($randomPosts as $randomPost)
        <div class="card text-center col-10 mx-auto mt-3">
            <div class="card-body">
                <h2 class="card-title mb-3"><a href="/posts/{{$randomPost->id}}">{{$randomPost->title}}</a></h2>
                <h6 class="card-text mb-3">Author: {{$randomPost->user->name}}</h6>
            </div>
        </div>
        @endforeach
    @elseif(count($randomPosts)==0)
    <div class="card text-center col-10 mx-auto mt-3">
        <div class="card-body">
            <h2 class="card-title mb-3">NO POST TO FEATURE</h2>
            <h6 class="card-text mb-3">Here you can see 3 randomly selected post from our users</h6>
        </div>
    </div>
    @endif
@endsection

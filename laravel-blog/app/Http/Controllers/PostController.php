<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//access the authenticated user via the Auth facade
use Illuminate\Support\Facades\Auth;

use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;
class PostController extends Controller
{

    //Controller is for the user logout
    public function logout(){
        // it will logout the currently logged in user.
        Auth::logout();
        return redirect('/login');
    }

    //action to return a view containing a form for a blog post creation
    public function createPost(){
        return view('posts.create');
    }

    public function savePost(Request $request){
        // to check whether there is an authenticated user:
        if(Auth::user()){
                        // instatiate a new Post Object from the Post method and then save it ib $post variable:
                        $post = new Post;

                        // define the properties of the $post object using the received form data
                        $post->title=$request->input('title');
                        $post->body=$request->input('content');

                        // this will get the id of the authenticated user and set it as the foreign jet user_id if the new post.
                        $post->user_id=(Auth::user()->id);
                        // save the post object in our Post Table;

                        $post->save();
                        return redirect('/posts')->with('create', "Post Created");
        }else{
            return redirect('login');
        }

    }

    // controller will return all the blog posts
    public function showPosts(){
        // the all() method will save all the records in our Post Table in our $post variable
        $posts = Post::where('isActive', true)->get();

        return view('posts.showPosts')->with('posts', $posts);
    }

    // this controller is for returning random post
    public function getRandomPost(){
        $randomPosts = Post::where('isActive', true)->inRandomOrder()->take(3)->get();
        return view('welcome')->with('randomPosts', $randomPosts);
    }

    // this controller is for showing only the posts authored by authenticated user
    public function myPosts(){
        if(Auth::user()){

            $posts = Auth::user()->posts;

            return view('posts.showPosts')->with('posts', $posts);
        }else{
            return redirect('/login');
        }
    }


    //action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown
        public function show($id){

        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    // controller for editing specific post of authenticated user
        public function edit($id){
            $post = Post::find($id);
            return view('posts.edit')->with('post', $post);
        }

    // Controller for updating post
        public function update(Request $request, $id){
            $post = Post::find($id);

            if (!$post) {
            return back()->with('error', 'Something went wrong please try again');
        }else{
            $post->title = $request->input('title');
            $post->body = $request->input('body');
            $post->save();
            return redirect('myPosts')->with('update', "Successfully updated");
        }
    }

    // Controller for archiving post
    public function archiveUnarchive($id){
        $post = Post::find($id);

        if(!$post){
            return back()->with('error', 'Something went wrong please try again');
        }elseif($post->isActive){
            $post->update(['isActive' => false]);
            return redirect('myPosts') ->with('archived', "Successfully archived");
        }elseif($post->isActive == false){
            $post->update(['isActive' => true]);
            return redirect('myPosts') ->with('unArchived', "Successfully UnArchived");
        }
    }

    // Controller for liking post
    public function like($id){
        $post = Post::find($id);

        if($post->user_id !=Auth::user()->id){
            if($post->likes->contains('user_id', Auth::user()->id)){
                PostLike::where('post_id', $post->id)->where('user_id', Auth::user()->id)->delete();
            }else{
                //create a new like record to like this post
                // instantiate a new PostLike object from the PostLike model
                $postLike = new PostLike;
                $postLike->post_id = $post->id;
                $postLike->user_id = Auth::user()->id;

                // save this postlike object in the database
                $postLike->save();
            }
        }
        return redirect("/posts/$id");
    }

    // Controller for comment
    public function comment($id, Request $request){
        if(Auth::user()){
            $post = Post::find($id);

            if ($post) {
                $postComment = new PostComment();
                $postComment->body = $request->input('comment');
                $postComment->post_id = $post->id;
                $postComment->user_id = Auth::user()->id;
                $postComment->save();
                return redirect()->back()->with('comment', 'Comment added successfully.');
            }else{
                return back()->with('error', 'Something went wrong please try again');
            }
        }
    }

}

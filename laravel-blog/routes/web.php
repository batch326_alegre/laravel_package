<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


use App\Http\Controllers\PostController;
use App\Models\Post;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $posts = Post::all();
    return view('welcome')->with('posts', $posts);
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//This route is for logging out.
Route::get('/logout', [PostController::class, 'logout']);

//This route is for creation of a new post:
Route::get('/posts/create', [PostController::class, 'createPost']);

// This route is for saving the post on our database
Route::post('/posts', [PostController::class, 'savePost']);

// This route is for the list of post on our databases:
Route::get('/posts', [PostController::class, 'showPosts']);

// This route is for the list of Random post from our database:
Route::get('/', [PostController::class, 'getRandomPost']);

// Define a route that will return a view containing only the authenticated user's post
Route::get('/myPosts', [PostController::class, 'myPosts']);

//define a route wherein a view showing a specific post with matching URL parameter ID will be returned to the user
Route::get('/posts/{id}', [PostController::class, 'show']);

// This route is for editing specific post of authenticated user
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// This route is for updating specific post of authenticated user
Route::put('/post/{id}', [PostController::class, 'update']);

// This route is for archiving/Unarchiving specific post of authenticated user
Route::get('/posts/{id}/archiveUnarchive', [PostController::class, 'archiveUnarchive']);

// Define a web route that will call the function for liking and unliking  a specific route
Route::put('/posts/{id}/like', [PostController::class, 'like']);

// Route for adding comments
Route::post('/posts/{id}/comment', [PostController::class, 'comment']);
